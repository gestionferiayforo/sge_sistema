<?php  
  include(dirname(__FILE__, 1)."/Controlador/General/CEvento.php");  

  $vlocResultadoVerificacionExistenciaEvento = FunVerificarExistenciaEventoSegunAñoActual();  
  if($vlocResultadoVerificacionExistenciaEvento == CteExisteEventoEnAñoActual){
    
    // header('Location: '.dirname(__FILE__, 1).'/Vista/General/IndexConEvento.php');//Aqui lo redireccionas al lugar que quieras.
    header('Location: Vista/General/IndexConEvento.php');//Aqui lo redireccionas al lugar que quieras.    
  }else{
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="../SGE_V1/Assets/imagenes/Recursos/Logo_UNI.png" height="30px" width="30px">
    <link rel="stylesheet" href="../SGE_V1/Assets/css/General/bootstrap.min.css">

    <link rel="stylesheet" href="../SGE_V1/Assets/herramientas/font-awesome-4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="../SGE_V1/Assets/css/General/index_srse.css">             
    
    <title>Inicio SGE-FCYS</title>
</head>
<body>
  <header>
      <div id="divPrincipalHeader">    
          <div class="logo">
          <img src="../SGE_V1/Assets/imagenes/Recursos/FCyS balnco.png" height="50px">
          </div>
          <ul class="nav justify-content-end">
              <li class="nav-item"><a class="nav-link" id="texto" href="" >Inicio</a></li>
              <li class="nav-item"><a class="nav-link" id="texto" href="../SGE_V1/Vista/General/Eventos_SR.php">Eventos</a></li>
              <li class="nav-item"><a class="nav-link" id="texto" href="../SGE_V1/Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
              <button class="buttonIniciarSesion"><a href="../SGE_V1/Vista/General/Iniciar_Sesion.php" id="texto">Iniciar Sesión</a></button>
          </ul>
      </div>

      <!--Inicio Menu Móvil-->
      <div class="main-header">
          <!-- <img id="imgLogUsuario" src="http://localhost/sge_sistema/ConstruccionSistema/SGE2/Assets/Imagenes/icono5.png">  -->
              <nav id="nav" class="main-nav">
              
                <div class="nav-links">                  
                  <!-- <a class="link-item-session" id="link-item-session2" href="#">Cerrar Sesión</a> -->
                  <a class="link-item"  href="" >Inicio</a>
                  <a class="link-item"  href="../SGE_V1/Vista/General/Eventos_SR.php">Eventos</a>
                  <a class="link-item"  href="../SGE_V1/Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a>                
                  <button><a href="../SGE_V1/Vista/General/Iniciar_Sesion.php" id="texto">Iniciar Sesión</a></button>
                </div>
              </nav>
              
              <button id="button-menu" class="button-menu">
                <span></span>
                <span></span>
                <span></span>
              </button>
      </div>
      <!--Fin Menu Móvil-->
  </header>
    <!--Para reidirigir a la página de la UNI-->
    <a href="https://www.uni.edu.ni/#/"><section id="sectionLinkUni">
      <img class="linkUni" rel="icon" src="../SGE_V1/Assets/imagenes/Recursos/Logo_UNI.png" height="25px" width="30px">
      <p class="linkUni">Ir a uni.edu.ni</p>
    </section></a>
    <!--=====================================-->

    <div id="fondo">
      <p id="h1_tituloinicio"> Sistema de Gestión de Eventos FCYS</p>
    </div>
<!--========== INICIO CARRUSEL ==========-->    
<div class="slideshow-container" >
  <div class="mySlides fade1" >
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/jornada_uni.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <div class="mySlides fade1">
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/calendario_actividades.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <div class="mySlides fade1">
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/uni_farq.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)">❯</a>
</div>
<br>

<div class="divTreePoints">
  <span class="dot" onclick="currentSlide(1)"></span>
  <span class="dot" onclick="currentSlide(2)"></span>
  <span class="dot" onclick="currentSlide(3)"></span>
</div> 
<br>  
<!--========== FIN CARRUSEL ========== -->


<!--========== INICIO NOTICIAS ======= -->
<div id="div_Contenedor_Noticias">
  <div id="div_Contenedor_Noticia_Principal">
      <img src="../SGE_V1/Assets/imagenes/Noticias/Notice1.png">
      <br><br><br><br>
      <h3></h3>
      <h5></h5>
      <div class="div_Animacion" ><h3 style="color:white;" class="h3_Titulo_Noticia">UNI campeona de Liga de Softball Hugo Chávez</h3></div>
  </div>
  <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Secundario">
      <img src="../SGE_V1/Assets/imagenes/Noticias/Noti2.png">
      <br><br><br><br>
      <h3></h3>
      <h5></h5>
      <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Curso de inducción a estudiantes en UNI - IES</h3></div>
  </div>
    <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Terciario">
      <img src="../SGE_V1/Assets/imagenes/Noticias/Noti3.png">
      <br><br><br><br>
      <h3></h3>
      <h5></h5>
      <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Estudiantes de nuevo ingreso, conocen el Mundo UNI 2023</h3></div>
  </div> 
</div>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  
<!--========== FIN NOTICIAS ======= -->  
   
   <!--INICIA CONSTRUCCION FOOTER-->
   <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6">
        <h2>Contáctenos</h2>
          <ul class="footer-links">
          <li><i class="fa fa-phone " ></i>+505 2249 6429</li>
              <li><i class=" fa fa-envelope-o  "></i></i>decanatura@fcys.uni.edu.ni</li>
              <li><i class=" fa fa-map-marker  "></i></i>Semáforos Villa Progreso 2 1/2 cuadras arriba</li>
          </ul>
        </div>

        <div class="col-xs-6 col-md-3">         
          <ul class="footer-links">
          <li><a href="">Inicio</a></li>
              <li><a href="../SGE_V1/Vista/General/Eventos_SR.php">Eventos</a></li>
              <li><a href="../SGE_V1/Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
          </ul>
        </div>

        <div class="col-xs-6 col-md-3" >
          <ul class="footer-links">
          <li><a href="../SGE_V1/Vista/General/Iniciar_Sesion.php">Iniciar Sesión</a></li>
          <li style="visibility: hidden;"><a href="../../Vista/VEvento/Eventos.html">Eventos</a></li>
          </ul>
        </div>

        <div class="col-xs-6">
          <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a class="global" href="#"><i class="fa fa-globe"></i></a></li>
          </ul>
        </div>
      </div>
      <hr>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-12">
          <p class="copyright-text"> &copy; Universidad Nacional De Ingenieria 2023 </p>
        </div>
                
      </div>
    </div>
</footer>
<script src="../SGE_V1/Assets/js/General/index_srse.js"></script> 
  <!--TERMINA CONSTRUCCION FOOTER-->
</body>


</html>
<?php
}
?>