<?php

session_start();

if ($_SESSION['PersonaAcademica']['ID_Tipo_Usuario'] != 4 && $_SESSION['PersonaAcademica']['ID_Tipo_Usuario'] != 6)  {


    header('Location: ../../Vista/General/Iniciar_Sesion.php');//Aqui lo redireccionas al lugar que quieras.
    die();

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="../../Assets/imagenes/Recursos/Logo_UNI.png" height="30px" width="30px">
    <link rel="stylesheet" href="../../Assets/css/General/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../Assets/herramientas/font-awesome-4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="../../Assets/css/Coordinador/Planificacion_Feria_CE.css">
    
    <title>Planificiacion Feria</title>
</head>
<body>
    <header>
        <div class="logo">
          <img src="../../Assets/imagenes/Recursos/FCyS balnco.png" height="50px">
        </div>
        <div class="menu_general">
          <ul class="nav justify-content-end">
            <li class="nav-item"><a class="nav-link active" id="texto" href="../../Vista/Coordinador/Index_Coordinador.php" >Inicio</a></li>
            <li class="nav-item"><a class="nav-link active" id="texto" href="../../Vista/Coordinador/Prox.php">Eventos</a></li>
            <li class="nav-item"><a class="nav-link active" id="texto" href="../../Vista/Coordinador/Admin_Feria_CE.php">Administracion de Eventos</a></li>
             
            <li><a href="">Comisiones </a>
					<ul>
                        <a id="FondoNav" href=".../../Vista/Coordinador/Prox.php">Comision Asignada</a>
                        <a id="FondoNav" href=".../../Vista/Coordinador/Prox.php">Comisiones Generales</a>
					</ul>
				</li>
            <li class="nav-item"><a class="nav-link active" id="texto" href=".../../Vista/Coordinador/Prox.php">Consolidados</a></li>
		
                <div class="dropdown">
                    <img src="<?php echo $_SESSION['Avatar']; ?>"  class="imgRedonda"/>
  
                    <div class="dropdown-content">
                        <a href="../../Vista/Coordinador/Prox.php">Mi Cuenta</a>
                        <a href='../../Controlador/General/CCerrarSesion.php'>Cerrar sesion</a>
                    </div>
                </div>
			</ul>
            <div class="Nombreusuario"><?php echo $_SESSION['NombreCompleto']; ?></div>
        </div>

        <!--A partir de aqui inicia el menu movil, pero copiar todo lo contenido en HEADER-->
        <div class="main-header">
        
            <nav id="nav" class="main-nav">
              <div class="nav-links">
              <img src="<?php echo $_SESSION['Avatar']; ?>"  class="imgRedonda link-item"/>
              <div class="NombreusuarioM"><?php echo $_SESSION['NombreCompleto']; ?></div>
        
              <a class="link-item"  href="../../Vista/Coordinador/Index_Coordinador.php">Inicio</a>
                <a class="link-item"  href="../../Vista/Coordinador/Prox.php">Eventos</a>
                <a class="link-item"  href="../../Vista/Coordinador/Admin_Feria_CE.php">Administracion de Eventos</a>
                <a class="link-item"  href="../../Vista/Coordinador/Prox.php">Comision Asignada</a>
                <a class="link-item"  href="../../Vista/Coordinador/Prox.php">Comisiones Generales</a>
                <a class="link-item"  href="../../Vista/Coordinador/Prox.php">Consolidados</a>
                <a class="link-item"  href="../../Vista/Coordinador/Prox.php">Mi Cuenta</a>
                <a class="link-item"  href='../../Controlador/General/CCerrarSesion.php'>Cerrar sesion</a>
                
              </div>
            </nav>
            <button id="button-menu" class="button-menu">
              <span></span>
              <span></span>
              <span></span>
            </button>
          </div>
    </header>
    
    <img src="../../Assets/imagenes/Recursos/mosaico1.png" id="mosaicoDER" height="180px" width="180px">
    <a class="nav-link active" id="texto_atras" href="javascript:history.back()" > << Atrás  </a>
    <h4 class="h4">Planificación de Evento Feria</h4>
    <h4 class="h4"> Etapas de Planificación</h4>


    <div class="card-deck w-75">
      
    <div class="card w-75">
  
  <div class="card-body- text-center">
  <img class="card-img-top" src="../../Assets/imagenes/Recursos/S_DatosG.png" >
    <!--<a href="../../Vista/Coordinador/PlanificacionE1.php" class="btn btn-primary">Datos Generales</a>-->
    <button onclick="location.href='../../Vista/Coordinador/Prox.php'" class="btn btn-primary">Datos Generales</button>  </div>
</div>

<div class="card w-75">
  <div class="card-body- text-center">
  <img class="card-img-top" src="../../Assets/imagenes/Recursos/S_Comision.png" >
    <!--<a href="#" class="btn btn-primary" >Gestionar Comisiones</a>-->
    <button onclick="location.href='../../Vista/Coordinador/PlanificacionE2.php'" class="btn btn-primary">Gestionar Comisiones</button>  
  </div>
</div>
<div class="card w-75">
  <div class="card-body- text-center">
  <img class="card-img-top" src="../../Assets/imagenes/Recursos/S_CatSub.png" >
      <!--<button href="#" class="btn btn-primary" disabled >Gestionar Categorias y Subcategorias</button>-->
      <button onclick="location.href='../../Vista/Coordinador/Prox.php'" class="btn btn-primary">Gestionar Categorias y Subcategorias</button>  
  </div>
</div>
<div class="card w-75">
  <div class="card-body- text-center">
  <img class="card-img-top" src="../../Assets/imagenes/Recursos/S_Conferencia.png" >
    <!--<a href="#" class="btn btn-primary">Gestionar Conferencias</a>-->
    <button onclick="location.href='../../Vista/Coordinador/Prox.php'" class="btn btn-primary">Gestionar Conferencias</button>  
    
    
  </div>
</div>
<div class="card w-75">
  <div class="card-body- text-center">
  <img class="card-img-top" src="../../Assets/imagenes/Recursos/S_Jurado.png" >
    <!--<a href="#" class="btn btn-primary">Gestionar Jurados</a>-->
    
    <button onclick="location.href='../../Vista/Coordinador/Prox.php'" class="btn btn-primary">Gestionar Jurados</button>  
  </div>
</div>
</div>

    
    <script src="../../Assets/js/General/menu_movil.js"></script>
        <br>
        <img src="../../Assets/imagenes/Recursos/mosaicos2.png" id="mosaicoIZQ" height="180px" width="180px">
        <br>
        
        


<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
          <h2>Contactenos</h2>
            <ul class="footer-links">
            <li><i class="fa fa-phone " ></i>+505 2249 6429</li>
                <li><i class=" fa fa-envelope-o  "></i></i>decanatura@fcys.uni.edu.ni</li>
                <li><i class=" fa fa-map-marker  "></i></i>Semáforos Villa Progreso 2 1/2 cuadras arriba</li>
            </ul>
          </div>
  
          <div class="col-xs-6 col-md-3">         
            <ul class="footer-links">
                <li><a href="../../Vista/Coordinador/Index_Coordinador.php">Inicio</a></li>
                <li><a href="../../Vista/Coordinador/Prox.php">Eventos</a></li>
                <li><a href="../../Vista/Coordinador/Admin_Feria_CE.php">Administracion de Eventos</a></li>
                <li><a href=".../../Vista/Coordinador/Prox.php">Mi cuenta</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <ul class="footer-links">
       
            <li><a href="../../Vista/Coordinador/Prox.php">Comision Asignada</a></li>
            <li><a href="../../Vista/Coordinador/Prox.php">Comisiones Generales</a></li>
            <li><a href="../../Vista/Coordinador/Prox.php">Consolidados</a></li>
            
            </ul>
          </div>

          <div class="col-xs-6">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="global" href="#"><i class="fa fa-globe"></i></a></li> 
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text"> &copy; Universidad Nacional De Ingenieria 2023 </p>
          </div>
          

          <!--<div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="global" href="#"><i class="fa fa-globe"></i></a></li>
             
            </ul>
          </div>-->
        </div>
      </div>
</footer>   