<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="../../Assets/imagenes/Recursos/Logo_UNI.png" height="30px" width="30px">
    <link rel="stylesheet" href="../../Assets/css/General/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../Assets/herramientas/font-awesome-4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="../../Assets/css/General/evento_SR.css">   
   
    <title>Eventos SGE-FCYS</title>
</head>
<body>
    <header>
        <div class="logo">
            <img src="../../Assets/imagenes/Recursos/FCyS balnco.png" height="50px">
        </div>
        <div class="menu_general">
            <ul class="nav justify-content-end">
                <li class="nav-item"><a class="nav-link active" id="texto" href="../../index_SRSE.php" >Inicio</a></li>
                <li class="nav-item"><a class="nav-link active" id="texto" href="../../Vista/General/Eventos_SR.php">Eventos</a></li>
                <li class="nav-item"><a class="nav-link active" id="texto" href="../../Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
                <button><a href="../../Vista/General/Iniciar_Sesion.php" id="texto">Iniciar Sesión</a></button>
            </ul>
        </div>

     <!--Inicia el menu movil-->
        <div class="main-header">
            
            <nav id="nav" class="main-nav">
              <div class="nav-links">
                <a class="link-item"  href="../../index_SRSE.php" >Inicio</a>
                <a class="link-item"  href="../../Vista/General/Eventos_SR.php">Eventos</a>
                <a class="link-item"  href="../../Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a>
                <button><a href="../../Vista/General/Iniciar_Sesion.php" id="texto">Iniciar Sesión</a></button>
              </div>
            </nav>
            <button id="button-menu" class="button-menu">
              <span></span>
              <span></span>
              <span></span>
            </button>
        </div>

    </header>
    <img src="../../Assets/imagenes/Recursos/mosaico1.png" id="mosaicoDER" >
    <h4 class="h4">Eventos de la Facultad</h4>
        <div class="cuadro_fondo">
            <div class="cuadro1">
                <h5>Ferias</h5>
                <p>Actividad donde se exponen proyectos de las diferentes asignaturas
                    que se imparten en la carrera de Ingeniería de Sistemas clasificadas 
                    por categorías, en la cual se aplican los conocimientos adquiridos.                    
                </p>
            </div>
            <div class="cuadro2">
                <h5>Foros</h5>
                <p>Actividad en la cual personas capacitadas en las diferentes áreas de
                    la Facultad, exponen sobre sus experiencias relacionadas al contenido
                    de la carrera.
                </p>
            </div>
            <div class="cuadro3">
                <h5>Congresos</h5>
                <p>Actividad donde se desarrollan conferencias con profesionales, donde 
                    imparten conocimientos de interés en temáticas vinculadas a la
                    carrera de Ingeniería de Sistemas.
                </p>
            </div>
        </div>
        <h4 class="h4">Historia de los Eventos</h4>
        <div class="cuadro_fondo2">
            <p class="cuadro">La Facultad de Ciencias y Sistemas ha gestionado de diferentes maneras estos
                eventos, desde la apertura de las ferias, siendo la primera realizada en 1999,
                hasta la más reciente efectuada en el 2019, asi como la creación del Primer 
                Congreso Nacional de Ingeniería de Sistemas realizado en el año 2017, y el
                desarrollo de foros, siendo el Primer Foro Nacional de Matemática realizado 
                en el año 2009.
            </p>
        </div>
        
        <img src="../../Assets/imagenes/Recursos/mosaicos2.png" id="mosaicoIZQ" height="180px" width="180px"> 
        
        <script src="../../Assets/js/General/menu_movil.js"></script>
        <br>
        
    
        <footer class="site-footer">
            <div class="container">
              <div class="row">
                <div class="col-sm-12 col-md-6">
                <h2>Contáctenos</h2>
                  <ul class="footer-links">
                  <li><i class="fa fa-phone " ></i>+505 2249 6429</li>
                      <li><i class=" fa fa-envelope-o  "></i></i>decanatura@fcys.uni.edu.ni</li>
                      <li><i class=" fa fa-map-marker  "></i></i>Semáforos Villa Progreso 2 1/2 cuadras arriba</li>
                  </ul>
                </div>
      
                <div class="col-xs-6 col-md-3">
                 
                  <ul class="footer-links">
                  <li><a href="../../index_SRSE.php">Inicio</a></li>
                      <li><a href="../../Vista/General/Eventos_SR.php">Eventos</a></li>
                      <li><a href="../../Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
                  </ul>
                </div>
                
              </div>
              <hr>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <p class="copyright-text"> &copy; Universidad Nacional De Ingenieria 2022 </p>
                </div>
      
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <ul class="social-icons">
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="global" href="#"><i class="fa fa-globe"></i></a></li>
                   
                  </ul>
                </div>
              </div>
            </div>
          </footer>
</body>
</html>
  