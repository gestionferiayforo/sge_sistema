<?php
  require_once ("../../Controlador/General/CUsuario.php"); 
  require_once(dirname(__FILE__, 3)."/Controlador/General/CEvento.php");  
  require_once ("../../Controlador/General/helper.php"); 

  session_start();

  //Para verificar la existencia del evento segun el año el actual
  $vlocResultadoVerificacionExistenciaEvento = FunVerificarExistenciaEventoSegunAñoActual();
    // echo "Prueba Samir: ".$vlocResultadoVerificacionExistenciaEvento;
    // exit;
  if($vlocResultadoVerificacionExistenciaEvento != CteExisteEventoEnAñoActual){
    header('Location: ../../Vista/Participante/InicioParticipanteSinEvento.php');//Aqui lo redireccionas al lugar que quieras.        
    die();
  }
  
  //Para verificar si el participante inicio sesión
  if (!isset($_SESSION['Participantes']) or $_SESSION['Participantes']['ID_Tipo_Usuario']  != "1")  {
    header('Location: ../../Vista/General/Iniciar_Sesion.php');//Aqui lo redireccionas al lugar que quieras.
    die();  
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../../Assets/imagenes/Recursos/Logo_UNI.png" height="30px" width="30px">
  <link rel="stylesheet" href="../../Assets/css/General/bootstrap.min.css">

  <link rel="stylesheet" href="../../Assets/herramientas/font-awesome-4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="../../Assets/css/Participante/inicioParticipanteConEvento.css">
  
  <title>Inicio Participante CE</title>
</head>
<body>
    <header>
        <div>    
          <div class="logo">
            <img src="../../Assets/imagenes/Recursos/FCyS balnco.png" height="50px">            
          </div>
          <ul class="nav justify-content-end" id="ulNavHeader">
            <li ><a class="nav-link" id="texto" href="../../Vista/Participante/inicioParticipanteSinEvento.php" >Inicio</a></li>
            <li ><a class="nav-link" id="texto" href="../../Vista/Participante/Eventos_PSE.php">Eventos</a></li>
            <li ><a class="nav-link" id="texto" href="../../Vista/Participante/QueEs_InfoSisP.php">¿Qué es SGE-FCYS?</a></li>            
          </ul>                    
          <section id= "sectionCentralizarLogoNombre">
            <img id="imgLogUsuario" class="imgLogUsuarioClass" src="<?php echo $_SESSION['Avatar']; ?>">  
            <br>
            <a id="aNombreUsuario"><?php echo $_SESSION['NombreCompleto']; ?></a>
          </section>                            
          <div id="divMenuDespliegue">              
            <a class="nav-link " href="../../Vista/Participante/MiCuenta_SE_v.php">Mi Cuenta</a>
            <a class="nav-link " id="aCerrarSesion" href="../../Controlador/General/CCerrarSesion.php">Cerrar Sesión</a>
          </div>
        </div>
        
        <!--Inicio Menu Móvil-->
        <div class="main-header">          
            <nav id="nav" class="main-nav" >            
              <div class="nav-links"> 
                <img id="imgLogUsuarioMovil" src="<?php echo $_SESSION['Avatar'] ?>">                                  
                <div class="Nombreusuario"><?php echo $_SESSION['NombreCompleto']; ?></div>
                <a class="link-item"  href="../../Vista/Participante/inicioParticipanteSinEvento.php" >Inicio</a>
                <a class="link-item"  href="../../Vista/Participante/Eventos_PSE.php">Eventos</a>
                <a class="link-item"  href="../../Vista/Participante/QueEs_InfoSisP.php">¿Qué es SGE-FCYS?</a>    
                <a class="link-item" id="link-item-session2" href="../../Controlador/General/CCerrarSesion.php" >Cerrar Sesión</a>
                <!-- <a class="link-item"  href=".../../Vista/Participante/MiCuenta_SE_v.php">Mi Cuenta</a>              -->
              </div>
            </nav>
            
            <button id="button-menu" class="button-menu">
              <span></span>
              <span></span>
              <span></span>
            </button>
        </div>
        <!--Fin Menu Móvil-->
    </header>

    <!--Para reidirigir a la página de la UNI-->
    <a href="https://www.uni.edu.ni/#/"><section id="sectionLinkUni">
      <img class="linkUni" rel="icon" src="../../../SGE_V1/Assets/imagenes/Recursos/Logo_UNI.png" height="25px" width="30px">
      <p class="linkUni">Ir a uni.edu.ni</p>
    </section></a>
    <!--=====================================-->

    <!--=== INICIO ENCABEZADOS ===-->
    <button id="butInscribirAEvento"><a href="../../Vista/Participante/InscripcionEventoFeria.php" id="texto">Inscribir a Evento</a></button>
    <br><br>
      <!--=== INICIO TEMPORIZADOR ===-->
      <section id="divTemporizador">
        <div class="divDato" id="divDatoDias">
            <h2 class="h2Encabezado">Días</h2>
            <h2 class="h2Digito" id="h2DigitoDias"><?php echo func_get_days_for_event(); ?></h2>
        </div>            
        <div class="divDato" id="divDatoHoras">
            <h2 class="h2Encabezado">Horas</h2>
            <h2 class="h2Digito" id="h2DigitoHoras"><?php echo func_get_hours_for_event(); ?></h2>
        </div>
        
        <div class="divDato">
            <h1 class="aDosPuntos" id="dosPuntosMinutos">:</h1>
            <h2 class="h2Encabezado">Minutos</h2>
            <h2 class="h2Digito"id="h2DigitoMinutos"><?php echo func_get_minutes_for_event(); ?></h2>
        </div>
        
        <div class="divDato">
          <h1 class="aDosPuntos">:</h1>
          <h2 class="h2Encabezado">Segundos</h2>
          <h2 class="h2Digito"id="h2DigitoSegundos"><?php echo func_get_seconds_for_event(); ?></h2>
        </div>          
      </section>
      <!--=== FIN TEMPORIZADOR ===-->        

    <div id="divTituloInicial">
      <p class="pTituloInicial"> Feria de Ciencia y Tecnología </p>
      <p class="pTituloInicial"> <?php echo FunObtenerDiaEventoActual(); ?> de <?php echo FunObtenerMesEventoActualEnLetras(); ?>, UNI-Rupap </p>
    </div>
    <!--=== FIN ENCABEZADOS ===-->    

<!--========== INICIO NOTICIAS ======= -->
<div id="div_Contenedor_Noticias">
  <div id="div_Contenedor_Noticia_Principal">
      <img src="../../Assets/imagenes/Noticias/Noti1.png">
      <br><br><br><br>
      <h3></h3>
      <h5></h5>
      <div class="div_Animacion" id="divTituloNoticiaPrincipal"><h3 style="color:white;" class="h3_Titulo_Noticia">Se consolida la conciencia de prevención</h3></div>
  </div>
  <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Secundario">
      <img src="../../Assets/imagenes/Noticias/Noti2.png">
      <br><br><br><br>
      <h3></h3>
      <h5></h5>
      <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Curso de inducción a estudiantes de UNI - IES 2023</h3></div>
  </div>
  <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Terciario">
    <img src="../../Assets/imagenes/Noticias/Noti3.png">
    <br><br><br><br>
    <h3></h3>
    <h5></h5>
    <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Estudiantes de nuevo ingreso, conocen el mundo UNI 2023</h3></div>
  </div> 
</div>

<!--========== FIN NOTICIAS ======= -->

<!--========== INICIO LINEA DE TIEMPO DEL EVENTO ======= -->
<div id="divLineaTiempoEvento">
  <ul id="ulMenuLineaTiempo">
    <li><a href="#" class="liMenuLineaTiempo">Visitas a las sedes</a></li>
    <li><a href="#" class="liMenuLineaTiempo">Periodo de inscripción</a></li>
    <li><a href="#" class="liMenuLineaTiempo">Periodo de Confirmación</a></li>
  </ul>

  <section class="secLineaDeTiempoEvento">
    <h3>Linea de Tiempo del Evento</h3>    
  </section>

  <section class="secLineaDeTiempoEvento" id="secIdLineaDeTiempoEvento">
    <h3>Categorías del evento</h3>    
  </section>
</div>
<!--========== FIN LINEA DE TIEMPO DEL EVENTO ======= -->

   <script src="../../Assets/js/General/temporizador.js"></script> 
   <script src="../../Assets/js/Participante/inicioParticipanteConEvento.js"></script>

    <!--INICIA CONSTRUCCION FOOTER-->
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
      
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h2>Contáctenos</h2>
            <ul class="footer-links">
              <li><i class="fa fa-phone " ></i>+505 2249 6429</li>
              <li><i class=" fa fa-envelope-o  "></i></i>decanatura@fcys.uni.edu.ni</li>
              <li><i class=" fa fa-map-marker  "></i></i>Semáforos Villa Progreso 2 1/2 cuadras arriba</li>
            </ul>
          </div>
  
          <div class="col-xs-6 col-md-3">         
            <ul class="footer-links">
              <li><a href="../../Vista/Participante/inicioParticipanteSinEvento.php">Inicio</a></li>
              <li><a href="../../Vista/Participante/Eventos_PSE.php">Eventos</a></li>
              <li><a href="../../Vista/Participante/QueEs_InfoSisP.php">¿Qué es SGE-FCYS?</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <ul class="footer-links">
              <li><a href="../../Vista/Participante/MiCuenta_SE_v.php">Mi cuenta</a></li>
              <li style="visibility: hidden;"><a href="../../Vista/VEvento/Eventos.html">Eventos</a></li>
            </ul>
          </div>

          <div class="col-xs-6">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="global" href="#"><i class="fa fa-globe"></i></a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text"> &copy; Universidad Nacional De Ingenieria 2023 </p>
          </div>                 
        </div>
      </div>
    </footer>
    <!--TERMINA CONSTRUCCION FOOTER-->

</body>

</html>