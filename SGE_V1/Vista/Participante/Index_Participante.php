
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="../SGE_V1/Assets/imagenes/Recursos/Logo_UNI.png" height="30px" width="30px">
    <link rel="stylesheet" href="../SGE_V1/Assets/css/General/bootstrap.min.css">
    <link rel="stylesheet" href="../SGE_V1/Assets/herramientas/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../SGE_V1/Assets/css/General/index.css"> 
    
    <title>Inicio SGE P</title>
</head>
<body>
    <header>
        <div>    
            <div class="logo">
            <img src="../SGE_V1/Assets/imagenes/Recursos/FCyS balnco.png" height="50px">
            </div>
            <ul class="nav justify-content-end">
                <li class="nav-item"><a class="nav-link " id="texto" href="" >Inicio</a></li>
                <li class="nav-item"><a class="nav-link " id="texto" href="">Eventos</a></li>
                <li class="nav-item"><a class="nav-link " id="texto" href="../SGE_V1/Vista/General/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
                <button><a href="" id="texto">Iniciar Sesión</a></button>
            </ul>
        </div>

        <!--Inicio Menu Móvil-->
        <div class="main-header">
            <!-- <img id="imgLogUsuario" src="http://localhost/sge_sistema/ConstruccionSistema/SGE2/Assets/Imagenes/icono5.png">  -->
                <nav id="nav" class="main-nav">
                
                  <div class="nav-links">
                    
                    <!-- <a class="link-item-session" id="link-item-session2" href="#">Cerrar Sesión</a> -->
                    <a class="link-item"  href="../../index.php" >Inicio</a>
                    <a class="link-item"  href="../../Vista/VEvento/Eventos.html">Eventos</a>
                    <a class="link-item"  href="../../Vista/VQueessge/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a>                
                    <button><a href="Inicio_Docente.html" id="texto">Iniciar Sesión</a></button>
                  </div>
                </nav>
                
                <button id="button-menu" class="button-menu">
                  <span></span>
                  <span></span>
                  <span></span>
                </button>
        </div>
        <!--Fin Menu Móvil-->
    </header>
        
    <div id="fondo">
      <p id="h1_tituloinicio"> Sistema de Gestión de Eventos FCYS</p>
    </div>
<!--========== INICIO CARRUSEL ==========-->    
<div class="slideshow-container">
  <div class="mySlides fade1">
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/jornada_uni.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <div class="mySlides fade1">
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/navidad_uni.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <div class="mySlides fade1">
      <div class="numbertext"></div>
      <img src="../SGE_V1/Assets/imagenes/Noticias/uni_tv.jpg" style="width:100%;">
      <div class = "text">  </div>
  </div>

  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)">❯</a>
</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span>
  <span class="dot" onclick="currentSlide(2)"></span>
  <span class="dot" onclick="currentSlide(3)"></span>
</div> 
<br>  
<!--========== FIN CARRUSEL ========== -->


<!--========== INICIO NOTICIAS ======= -->
<div id="div_Contenedor_Noticias">
    <div id="div_Contenedor_Noticia_Principal">
        <img src="../SGE_V1/Assets/imagenes/Noticias/Noti1.png">
        <br><br><br><br>
        <h3></h3>
        <h5></h5>
        <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Se consolida la conciencia de prevención</h3></div>
    </div>
    <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Secundario">
        <img src="../SGE_V1/Assets/imagenes/Noticias/Noti2.png">
        <br><br><br><br>
        <h3></h3>
        <h5></h5>
        <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Las pastorelas de la UNI son de Nicaragua</h3></div>
    </div>
     <div Class="div_Contenedor_Noticia_Secundario" id="div_Noticia_Terciario">
        <img src="../SGE_V1/Assets/imagenes/Noticias/Noti3.png">
        <br><br><br><br>
        <h3></h3>
        <h5></h5>
        <div class="div_Animacion"><h3 style="color:white;" class="h3_Titulo_Noticia">Rescatando la cultura ancestral Nic.</h3></div>
    </div> 
</div>

<!--========== FIN NOTICIAS ======= -->
   <script src="../SGE_V1/Assets/js/General/index.js"></script> 
   
   <!--INICIA CONSTRUCCION FOOTER-->
<div class="container"></div>
<footer >
  <!-- Footer main -->
    <section class="ft-main" >
        <div class="ft-main-item">
        <h2 class="ft-title">Contactenos</h2>
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-phone fa-1x" aria-hidden="true"></i>+505 2249 6429</li>
            <li><i class="fa-li fa fa-envelope-o  fa-1x" aria-hidden="true"></i></i>decanatura@fcys.uni.edu.ni</li>
            <li><i class="fa-li fa fa-map-marker  fa-1x" aria-hidden="true"></i></i>Semáforos Villa Progreso 2 1/2 cuadras arriba</li>
        </ul>
        </div>
        <div class="ft-main-item_2">
        <h2 class="ft-title"></h2>
        <ul>
            <li><a href="../../index.php">Inicio</a></li>
            <li><a href="../../Vista/VEvento/Eventos.html">Eventos</a></li>
            <li><a href="../../Vista/VQueessge/QueEs_InfoSis.html">¿Qué es SGE-FCYS?</a></li>
        </ul>
        
        </div>
        <!--
        <div class="ft-main-item_2">
        <h2 class="ft-title"></h2>
        <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Eventos</a></li>
            <li><a href="#">¿Qué es SGE-FCYS?</a></li>
        </ul>
        
        </div>-->
        <div class="ft-main-item">
        <h2 class="ft-title"></h2>
        <ul class="ft-social-list">
            <a href="#"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-globe fa-2x" aria-hidden="true"></i></a>
        </ul>
        </div>
    
    </section>
  <section class="ft-legal">
    <ul class="ft-legal-list">
      <li>&copy; Universidad Nacional De Ingenieria 2022</li>
    </ul>
  </section>
</footer>

  <!--TERMINA CONSTRUCCION FOOTER-->
</body>


</html>
