<?php

require_once ("../../Modelo/General/Conexionbd.php");

class EditarCuentaModelo {

    public function ListarGrupo($idsede, $idgrupo){
       
        $data = '';
        $query = "CAll Listar_Grupo_Participante($idsede,$idgrupo);";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }

    public function ActualizarDatos($id, $telefono, $correo, $grupo){

        $query = "CALL Actualizar_Datos_Participante('$id','$telefono', '$correo', $grupo);";         
        $mysqli= Conexiondatabase::ConexionSecurity();
        $consulta = $mysqli->query($query);
        return $consulta;
    }

    public function ListarDatosParticipante($idpersonaP){

        $datos = '';
        $query = "Call Cargar_Acceso_Participante($idpersonaP);";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);

        if ($result != null){
            if ($result -> num_rows == 1) {
                $datos = $result->fetch_assoc();

            }
        }
        $mysqli->close();
        return $datos;
    }

    public function BuscarRegistro($id,$tel,$correo){
        $dato='';

        if($query = "CALL BuscarRegistroTel('$id','$tel');")
        {
            $mysqli= Conexiondatabase::ConexionSecurity();
            $result = $mysqli->query($query);
            if($result){
                while($row = $result->fetch_array()){
                    if($row[0] > 0){

                        $dato = "Lo sentimos, este teléfono ya se encuentra registrado";
                    }
                $mysqli->close();
                }
            }
        } 
        
        if($query = "CALL BuscarRegistroC('$id','$correo');")
        {
            $mysqli= Conexiondatabase::ConexionSecurity();
            $result = $mysqli->query($query);
            if($result){
                while($row = $result->fetch_array()){
                    if($row[0] > 0){

                        $dato = "Lo sentimos, este correo ya se encuentra registrado";
                    }
                $mysqli->close();
                }
            }
           
        }

        return $dato;

    }

}


?>