<?php

require_once ("../../Modelo/General/Conexionbd.php");

class PlanificacionEM{


    public function get_insertar_evento($TipoE,$NombreE,$EsloganE,$nombreBD,$HoraE,$FechaE,$LugarE)
    {
        $insertp = '';
        $query = "CAll Insertar_DatosGeneralesEvento('$TipoE','$NombreE','$EsloganE','$nombreBD','$HoraE','$FechaE','$LugarE');";        
       
        //echo $query;
        //exit;
       
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if(!$result){
            $insertp = $mysqli->error;
        }else{
            $insertp = "1";
        }
        $mysqli->close();
        return $insertp;  
    }

    public function select_sitio()
    {
        $data = '';
        $query = "CAll Lista_Sitio();";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }

    public function select_comision()
    {
        $data = '';
        $query = "CAll Lista_Comision();";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }

    public function lista_funcioncomision($Id_Comision)
    {
        $data = '';
        $query = "CAll Lista_FuncionSegunComision($Id_Comision);";  // IMPRIME COMO TABLA*/       

        //$query = "CAll Lista_FuncionSegunComision_1($Id_Comision);";

        //echo ($query);
        //exit;

        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);

        
        if ($result) {
            while ($row = $result->fetch_array()) {
                $data .= $row[0];
            }
        }
           
        $mysqli->close();
        return $data;
    }

    public function select_PersonalAcademico()
    {
        $data = '';
        $query = "CAll Lista_PersonalAcemico();";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }

    public function Datos_TablaPAcedemico_Responsable($Id_Persona)
    {
        $data = '';
        $query = "CAll Obtener_NombrePAcademico_Responsable($Id_Persona);";  // IMPRIME COMO TABLA*/       


        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);

        
        if ($result) {
            while ($row = $result->fetch_array()) {
                $data .= $row[0];
            }
        }
           
        $mysqli->close();
        return $data;
    }

    public function Datos_TablaPAcedemico($Id_Persona)
    {
        $data = '';
        $query = "CAll Obtener_NombrePAcademico($Id_Persona);";  // IMPRIME COMO TABLA*/       

        //echo($query);
        //exit();

        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);

        
        if ($result) {
            while ($row = $result->fetch_array()) {
                $data .= $row[0];
            }
        }
           
        $mysqli->close();
        return $data;   

        /*if ($result && $result -> num_rows == 2) {
            $datosE = $result->fetch_assoc();


        }
        else { $datosE = NULL; }

        $mysqli->close();
        return $datosE;*/
    }

}