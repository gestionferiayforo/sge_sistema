<?php

require_once ("../../Modelo/General/Conexionbd.php");

class TipoUModel{

    public function select_tipoU()
    {
        $data = '';
        $query = "CAll Listar_TipoU();";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }

    public function select_sede($idsede,$idgrupo)
    {
        $data = '';
        $query = "CAll Prueb_select($idsede,$idgrupo);";
        $mysqli= Conexiondatabase::ConexionSecurity();
        $result = $mysqli->query($query);
        if($result){
            while ($valores = $result->fetch_array()){
                $data .= $valores[0];
            }
            $result ->close();
        };
           
        $mysqli->close();
        return $data;
    }
}

?>