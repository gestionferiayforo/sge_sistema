<?php

    // require_once ("../../Modelo/General/Conexionbd.php");        
    require_once (dirname(__FILE__, 2)."/General/Conexionbd.php");        

    Class EventoModelo{

        public function get_date_event($vparDateAño){
            $cons = '';
                $query = "CAll Obtener_FechaEventoFeriaSegunAño(".$vparDateAño.");";
                $mysqli= Conexiondatabase::ConexionSecurity();
                $result = $mysqli->query($query);
                if(!$result)
                    $cons = $mysqli->error;
                $mysqli->close();
                return $result;
        }    
        
        Public Function FunVerificarExistenciaEventoSegunAñoActual($vparDateAñoActual){
            $vlocIntVerificacion = "";
            $vlocStrQuery = "Call Verificar_ExistenciaEventoFeriaSegunAño(".$vparDateAñoActual.");";
            $vlocMysqli = Conexiondatabase::ConexionSecurity();
            $vlocResult = $vlocMysqli->query($vlocStrQuery);
            // echo "Prueba: ".$vlocResult->fetch_array(MYSQLI_BOTH)[0];
            // exit;
            if(!$vlocResult)
                $vlocIntVerificacion = $vlocMysqli->error;
            
            $vlocResultRow = $vlocResult->fetch_array(MYSQLI_BOTH)[0];
            // echo 'Prueba Samir MEvento.php: '.$vlocResult->fetch_array(MYSQLI_BOTH)[0];
            // exit;

            $vlocMysqli->close();
            return $vlocResultRow;
        }

    }        

?>