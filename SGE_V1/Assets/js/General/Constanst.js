/**Para inscripción a evento feria y confirmación de participantes */

    /**
     * Cuando se confirma el código del participante inscribiendo
     */
    const Cnt_Codigo_Corecto = 1;

    /**
     * Cuando se elimina el registro de confirmación de participante por tiempo exedido
     */
    const Cnt_Tiempo_Exedido = 1;

    /**
     * Existencia de registro de confirmación del participante
     */
    const Cnt_Existe_Registro_Confirmacion_Participante = 1;

    /**
     * La variable no es vacía
     */
    const Cnt_Contiene_Algun_Valor = 1;    

    /**
     * Cuando se registro el envío de mensaje
     */
    const Cnt_Registro_Envio_Mensaje_Exitoso = 1;

    /**
     * Para mostrar el botón de 'Cancelar' en los popups
     */
    const Cnt_Mostrar_Boton_Cancelar = true;

    /**Constantes para obtener los datos del participante a inscribir según la posición en el array */
        const Cnt_Primer_Nombre_Participante = 0;
        const Cnt_Segundo_Nombre_Participante = 1;
        const Cnt_Primer_Apellido_Participante = 2;
        const Cnt_Segundo_Apellido_Participante = 3;
        const Cnt_Cedula_Participante = 4
        const Cnt_Numero_Carnet_Participante = 5;
        const Cnt_Id_Grupo_Participante = 6;
        const Cnt_Id_Sede_Participante = 7;
        const Cnt_Telefono_Participante = 8;
        const Cnt_Correo_Electronico_Participante = 9;
        const Cnt_Codigo_Registro = 10;
    /*********************************************** */

    /**Constantes para indicar la posición de los inputs de los formularios para inscribir un proyecto al evento */
        const Cnt_Valor_Input_Nombres = 0;
        const Cnt_Valor_Input_Apellidos = 1;
        const Cnt_Valor_Input_Cedula = 2;
        const Cnt_Valor_Input_Carnet = 3;
        const Cnt_Valor_Input_Grupo = 4;
        const Cnt_Valor_Input_Sede = 5;
        const Cnt_Valor_Input_Telefono = 6;
        const Cnt_Valor_Input_Correo = 7;  
    /*********************************************** */
    
/*********************************************** */

/**Para inscribir Proyecto */

    /**
     * Cuando se obtiene el carnet del participante
     */
    const Cnt_Obtener_Carnet = 1;

    /**
     * Cuando un participante se puede inscribir en la sub categoría seleccionada.
     */
    const Cnt_Acceso_Permitido = 1;

    /**
     * Cuando se insertó correctamente la relación del evento con el proyecto
     */
    const Cnt_Se_Inserto_Evento_Proyecto = 1;
/*********************************************** */

/** Contantes Generales */
    /**
     * Para entrar a la ejecución en un ajax
     */
    const Cnt_Ejecutar_Ajax = 1;

    /**
     * La variable está vacía
     */
    const Cnt_Valor_Vacio = '';

    /**
     * La variable es Nula
     */
    const Cnt_Valor_Nulo = null;

    /**
     * Para color del botón de confirmación de una alerta SWAL (SweetAlert)
     */
    const Cnt_Color_Boton_Confirmacion = '#3085d6';

    /**
     * Para color del botón de cancelación de una alerta SWAL (SweetAlert)
     */
    const Cnt_Color_Boton_Cancelacion = '#d33';

    /**
     * Para evaluar si se ha confirmado la cancelación de la inscripcion
     */
    const Cnt_Cancelacion_Confirmada = true;
/*********************************************** */