let tiempoSegundos = document.getElementById("h2DigitoSegundos").innerHTML;
let tiempoMinutos = document.getElementById("h2DigitoMinutos").innerHTML;
let tiempoHoras = document.getElementById("h2DigitoHoras").innerHTML;
let tiempoDias = document.getElementById("h2DigitoDias").innerHTML;

cifrasInicialesTemporizador()
temporizar()

function cifrasInicialesTemporizador(){        
    funReferenciarComponentePorId("h2DigitoSegundos").innerHTML = tiempoSegundos
    funReferenciarComponentePorId("h2DigitoMinutos").innerHTML = tiempoMinutos
    funReferenciarComponentePorId("h2DigitoHoras").innerHTML = tiempoHoras
    funReferenciarComponentePorId("h2DigitoDias").innerHTML = tiempoDias
}

function temporizar(){    
    let contando = tiempoSegundos > 0        

    if(contando){
         
        tiempoSegundos = tiempoSegundos - 1
             
        funReferenciarComponentePorId("h2DigitoSegundos").innerHTML = tiempoSegundos + 1
        setTimeout(() => {
           temporizar() 
        }, 1000);
    }else if(tiempoMinutos > 0){        
        tiempoMinutos = tiempoMinutos - 1
        funReferenciarComponentePorId("h2DigitoMinutos").innerHTML = tiempoMinutos
        tiempoSegundos = 59
        funReferenciarComponentePorId("h2DigitoSegundos").innerHTML = tiempoSegundos
        setTimeout(() => {
            temporizar() 
         }, 1000);
    }else if(tiempoHoras > 0){
        tiempoHoras = tiempoHoras - 1
        funReferenciarComponentePorId("h2DigitoHoras").innerHTML = tiempoHoras
        tiempoMinutos = 59
        funReferenciarComponentePorId("h2DigitoMinutos").innerHTML = tiempoMinutos
        tiempoSegundos = 59
        funReferenciarComponentePorId("h2DigitoSegundos").innerHTML = tiempoSegundos
        setTimeout(() => {
            temporizar() 
         }, 1000);
    }else if(tiempoDias > 0){
        tiempoDias = tiempoDias -1
        funReferenciarComponentePorId("h2DigitoDias").innerHTML = tiempoDias
        tiempoHoras = 23
        funReferenciarComponentePorId("h2DigitoHoras").innerHTML = tiempoHoras
        tiempoMinutos = 59
        funReferenciarComponentePorId("h2DigitoMinutos").innerHTML = tiempoMinutos
        tiempoSegundos = 59
        funReferenciarComponentePorId("h2DigitoSegundos").innerHTML = tiempoSegundos
        setTimeout(() => {
            temporizar() 
         }, 1000);
    }else{
        window.alert("La feria ha comenzado!")
    }
}


function funReferenciarComponentePorId(id){
    return document.getElementById(id)
}