$(document).ready(function() {

    
    $(document).on('click', '#btnGuardarDatosG', function(e){

        event.preventDefault(); 

        let fichero = $('#LogoE')[0].files[0];
        let NombreFeria = $("#NombreE").val();
        let EsloganFeria = $("#EsloganE").val();
        let HoraE = $("#HoraE").val();
        let FechaE = $("#FechaE").val();
        let LugarE = $("#LugarE").val();

        //let nombre = fichero.name;
        //let tipofile = fichero.type;

        let datos = new FormData();
        datos.append('tFile', fichero);
        datos.append('tNombreE', NombreFeria);
        datos.append('tEsloganE', EsloganFeria);
        datos.append('tHoraE', HoraE);
        datos.append('tFechaE',FechaE );
        datos.append('tLugarE',LugarE);

        //console.log(nombre);
        console.log(NombreFeria);
        console.log(EsloganFeria);
        console.log(HoraE);
        console.log(FechaE);
        console.log(LugarE);


        var formulario = document.DatosGeneralesFeriaE1;
        
    if( formulario.NombreE.value == ""){
        document.getElementById("Alerta").innerHTML = '<div class="alert alert-danger"></a>Favor ingresar el Nombre de la Feria</div>';
        
        Swal.fire(
            'Advertencia',
            'Favor ingresar el Nombre de la Feria',
            'warning'
          )

        formulario.NombreE.focus();
        return false;
    }

    if( formulario.EsloganE.value == ""){
        document.getElementById("Alerta").innerHTML = '<div class="alert alert-danger">Favor ingresar el Eslogan de la Feria</div>';

        Swal.fire(
            'Advertencia',
            'Favor ingresar el Eslogan de la Feria',
            'warning'
          )
         
        formulario.EsloganE.focus();
        return false;
    }

    if (formulario.LugarE.value == "Seleccione el lugar del evento"){
        document.getElementById("Alerta").innerHTML = '<div class="alert alert-danger">Favor ingresar El lugar del evento</div>';
        
        Swal.fire(
            'Advertencia',
            'Favor ingresar El lugar del evento',
            'warning'
          )

        formulario.LugarE.focus();   
        return false;
    
    }else{
        document.getElementById("Alerta").innerHTML = "";
    }

        $.ajax({
            type: "POST",
            url: "../../Controlador/Coordinador/CPlanificacionE1.php",
            data: datos,
            contentType: false,
            processData: false,

            success: function(result) {
                if(result.length !==0  ){
                    
                      Swal.fire({
                        customClass: {
                          confirmButton: 'swalBtnColor',
                        },
                        title: "Evento Creado Exitosamente",
                        text: " ¿Desea continuar con la planificación?",
                        icon: 'success',
                        showCancelButton: true,
                        confirmButtonText: 'Si',
                        cancelButtonText: 'No',
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                         window.location.href = "../../Vista/Coordinador/Admin_Feria.php";
                        } else {
                            window.location.href = "../../Vista/Coordinador/Admin_Feria_CE.php";
                        }
                      });                }
                else{
                    swal("No Logrado")
                }
            }
        });
    });

    $(document).on('click', '#btnCancelarR', function(e){

        event.preventDefault();
    
        Swal.fire({
            customClass: {
              confirmButton: 'swalBtnColor',
            },
            text: " ¿Desea realmente cancelar el registro?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
          })
          .then((result) => {
            if (result.isConfirmed) {
             window.location.href = "../../Vista/Coordinador/Planificacion_Feria.php";
            }
        });
    });
});

function validarletras(parametro){
    var patron = /^[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s\t-]*$/;
    if(parametro.search(patron)){
        return false;
    }else{
        return true;
    }

}


//Swal.fire('Favor ingresar el Nombre de la Feria');
        //swal("Favor ingresar el Nombre de la Feria"); 